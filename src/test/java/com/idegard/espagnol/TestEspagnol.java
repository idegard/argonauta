package com.idegard.espagnol;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

public class TestEspagnol {

	/**
	 * @throws Exception
	 * Analiza archivo input.txt y genera archivo con resultado de las 25
	 * combinaciones de letras en palabras mas repetidas en el input.
	 * 
	 */
	@Test
	public void getCombinaciones() throws Exception {
		char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		TreeMap<String, Integer> mapa = new TreeMap<String, Integer>();
		for (char c : alphabet)
			for (char d : alphabet)
				mapa.put(c + "" + d, 0);

		PrintWriter pdfSalida = new PrintWriter("espagnol-result.txt", "UTF-8");

		BufferedReader br = new BufferedReader(new InputStreamReader(
				TestEspagnol.class.getResourceAsStream("input.txt")));

		String strLine;

		while ((strLine = br.readLine()) != null) {
			String[] result = strLine.split(StringUtils.SPACE);
			for (String token : result) {
				char[] cadena = StringUtils.stripAccents(token).toCharArray();
				for (int i = 0; i < cadena.length; i++) {
					if (Character.isAlphabetic(cadena[i])) {
						try {
							String m = cadena[i] + "" + cadena[i + 1];
							if (mapa.containsKey(m))
								mapa.put(m, mapa.get(m) + 1);
						} catch (Exception e) {
						}
					}
				}
			}
		}

		br.close();

		int contadorTop = 0;
		for (String a : sortByComparator(mapa).keySet()) {
			pdfSalida.println(a + "=" + mapa.get(a));
			if (contadorTop++ > 25)
				break;
		}
		pdfSalida.close();
	}

	private static Map<String, Integer> sortByComparator(
			Map<String, Integer> unsortMap) {
		List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(
				unsortMap.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
					Map.Entry<String, Integer> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it
				.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}
}
