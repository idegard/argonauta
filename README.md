Super proyecto Argonauta
=

El super proyecto argonauta es la "fase II" del proyecto general de Redes 
Neuronales. La primer fase, mejor conocida como "proyecto cefalopodo" sirvió
tanto para sentar bases de las ANN como probar de forma práctica su funcionamiento;
tambien fue vital al momento de plantearnos un obetivo mayor, es la semilla de ésta fase que lleva por nombre
**proyecto argonauta**.

El objetivo de la [fase I](http://repo.idegard.com/idegard/cefalopodo) 
consistió en entrenar y usar una ANN para clasificar idiomas, en él nos dimos cuenta
del potencial de la librería [Neuroph](http://neuroph.sourceforge.net) y 
su facilidad de uso. Si leiste por completo la descripción del 
[proyecto cefalopodo](http://idegard.info) recordarás que nos planteamos como 
objetivo hacer un *Un sitio que "adivine/sepa" quién lo manipula...*; Éste es el proyecto que desarrolla la idea.



Para la documentación completa visita la [Wiki](http://repo.idegard.com/idegard/argonauta/wikis/home)

