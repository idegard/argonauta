'use strict';

/**
 * @ngdoc function
 * @name siteApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the siteApp
 */
angular.module('siteApp')
  .controller('MainCtrl', function ($scope) {
	  var teclas = [];
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
	$scope.teclaPresionada = function($event) {
		teclas.push({
			key : $event.keyCode,
			time : $event.timeStamp
		});
	};
  });
